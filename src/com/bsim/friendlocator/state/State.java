package com.bsim.friendlocator.state;

public class State {
	public static int START = 1;
	
	public static int INVITE_RECEIVED = 2;
	
	public static int IN_SESSION = 3;
}
