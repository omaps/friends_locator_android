package com.bsim.friendlocator.restservices.requestmessage;

import java.util.List;

import com.bsim.friendlocator.data.Contact;

public class CreateNewSession {
	
	private String name;
	
	private String phone;
	
	private String description;
	
	private List<Contact> contacts;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
