package com.bsim.friendlocator.restservices.executor.remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ServerProxy {
	
	public JSONObject getJSONFromUrl(String url) {
		InputStream is = null;
		String json = "";
		JSONObject jObj = null;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		}
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		}
		return jObj;
	}
	
	public static JSONObject makeHttpRequest(String url, String method, List<NameValuePair> params) {
		InputStream is = null;
		String json = "";
		JSONObject jObj = null;
		try {
			if(method == "POST"){
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);
				httpPost.setEntity(new UrlEncodedFormEntity(params));
				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();
			} else if(method == "GET") {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				String paramString = URLEncodedUtils.format(params, "utf-8");
				url += "?" + paramString;
				HttpGet httpGet = new HttpGet(url);
				HttpResponse httpResponse = httpClient.execute(httpGet);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();
			}
		} catch (UnsupportedEncodingException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (IllegalStateException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("serverexception", "Error converting result " + e.toString());
		}
		
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("serverexception", "Error parsing data " + e.toString());
		}
		
		return jObj;
	}
	
	
	public static JSONObject createSession(String name, String description) {
		InputStream is = null;
		String json = "";
		JSONObject jObj = null;
		
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		try {
			HttpPost post = new HttpPost(RemoteUrls.CREATE_SESSION_URL);
			JSONObject dataRequest = new JSONObject();
            dataRequest.put("name", name);
			dataRequest.put("description", description);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("data", dataRequest.toString()));
			post.setEntity(new UrlEncodedFormEntity(params));
			response = client.execute(post);
			if (response != null) {
                is = response.getEntity().getContent(); 
            }
		} catch (UnsupportedEncodingException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (IllegalStateException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e("serverexception", e.getMessage());
			e.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("serverexception", "Error converting result " + e.toString());
		}
		
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("serverexception", "Error parsing data " + e.toString());
		}
		
		return jObj;
	}
	
	
}
