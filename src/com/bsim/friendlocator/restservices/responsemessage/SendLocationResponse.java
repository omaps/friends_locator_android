package com.bsim.friendlocator.restservices.responsemessage;

public class SendLocationResponse {
	
	private boolean success;
	
	private String aditionalInfo;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getAditionalInfo() {
		return aditionalInfo;
	}

	public void setAditionalInfo(String aditionalInfo) {
		this.aditionalInfo = aditionalInfo;
	}
}
