package com.bsim.friendlocator.restservices.responsemessage;

public class CheckSessionResponse {
	
	private boolean hasSession;
	
	private String sessionId;

	public boolean isHasSession() {
		return hasSession;
	}

	public void setHasSession(boolean hasSession) {
		this.hasSession = hasSession;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
