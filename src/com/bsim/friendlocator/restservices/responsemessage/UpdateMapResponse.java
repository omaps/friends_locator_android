package com.bsim.friendlocator.restservices.responsemessage;

import java.util.List;

import com.bsim.friendlocator.data.FriendLocation;

public class UpdateMapResponse {
	
	private boolean success;
	
	private String additionalInfo;
	
	private List<FriendLocation> friends;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public List<FriendLocation> getFriends() {
		return friends;
	}

	public void setFriends(List<FriendLocation> friends) {
		this.friends = friends;
	}
}
