package com.bsim.friendlocator.activity;

import java.util.ArrayList;
import java.util.List;

import com.bsim.friendlocator.R;
import com.bsim.friendlocator.constants.ActivtyRequestCode;
import com.bsim.friendlocator.constants.SharedPreferenceConstants;
import com.bsim.friendlocator.restservices.executor.DummyServer;
import com.bsim.friendlocator.restservices.executor.RemoteServer;
import com.bsim.friendlocator.restservices.executor.Server;
import com.bsim.friendlocator.restservices.requestmessage.AcceptSession;
import com.bsim.friendlocator.restservices.requestmessage.CheckSession;
import com.bsim.friendlocator.restservices.requestmessage.DeleteSessionRequest;
import com.bsim.friendlocator.restservices.requestmessage.SendLocationRequest;
import com.bsim.friendlocator.restservices.requestmessage.UpdateMapRequest;
import com.bsim.friendlocator.restservices.responsemessage.AcceptSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.CheckSessionResponse;
import com.bsim.friendlocator.restservices.responsemessage.UpdateMapResponse;
import com.bsim.friendlocator.state.State;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class StartActivity extends ActionBarActivity {
	
	private int state = State.START;
	private boolean sessionStartedByUser = false;
	private String sessionId;
	private String username;
	private String phoneNumber;
	
	private List<Button> currentDispalyedButtons = new ArrayList<Button>();
	
	private Server server;
	
	private LocationManager locationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_start,
					container, false);
			return rootView;
		}
	}
	
	private double latitude = 0;
	private double longitude = 0;
	private final LocationListener locationListener = new LocationListener() {
		@Override
	    public void onLocationChanged(Location location) {
	        longitude = location.getLongitude();
	        latitude = location.getLatitude();
	        sendLocation();
	    }

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}
	};
	
	private void addLocationListener() {
		if (locationManager != null) {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);
		}
	}
	
	private void removeLocationListener() {
		if (locationManager != null) {
			locationManager.removeUpdates(locationListener);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ActivtyRequestCode.FROM_NEW_SESSION_TO_START) {
			if (RESULT_OK == resultCode) {
				sessionStartedByUser = true;
				state = State.IN_SESSION;
				setButtionsAccordingToState();
				addLocationListener();
			}
			if (RESULT_CANCELED == resultCode) {
				String message = data.getExtras().getString("message");
				if (message != null) {
					Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
				}
			}
		}
		// onResume is called after onActivityResult
		storeAppState();
	}
	
	private void addButton(Button button) {
		currentDispalyedButtons.add(button);
	}
	
	private void makeButtonsVisibleInButtonsLayout() {
		LinearLayout startLayout = (LinearLayout) findViewById(R.id.buttonsLayout);
		for (Button button : currentDispalyedButtons) {
			startLayout.addView(button);
		}
	}
	
	private void updateUserData() {
		Intent intent = new Intent(StartActivity.this, CollectUserDataActivity.class); 
		startActivity(intent);
	}
	
	private void removeOldButtons() {
		for (Button button : currentDispalyedButtons) {
			button.setVisibility(View.GONE);
		}
		currentDispalyedButtons.clear();
	}
	
	private boolean anInvitationWasReceived() {
		CheckSession checkSession = new CheckSession();
		CheckSessionResponse response = server.checkSession(checkSession);
		if (response.isHasSession()) {
			sessionId = response.getSessionId();
			return true;
		} else {
			return false;
		}
	}
	
	private void deleteSeesionOnServer() {
		DeleteSessionRequest request = new DeleteSessionRequest();
		request.setSessionId(sessionId);
		server.deleteSession(request);
		sessionId = null;
	}
	
	private void checkIfAnInvitationWasSent() {
		Toast.makeText(getApplicationContext(), "Retriving data", Toast.LENGTH_SHORT).show();
		if (anInvitationWasReceived()) {
			state = State.INVITE_RECEIVED;
			setButtionsAccordingToState();
		}
	}
	
	private AcceptSession createAcceptSessionMessage() {
		AcceptSession result = new AcceptSession();
		result.setName(username);
		result.setPhone(phoneNumber);
		result.setSessionId(sessionId);
		return result;
	}
	
	private void acceptInvitation() {
		AcceptSessionResponse response = server.acceptSession(createAcceptSessionMessage());
		if (response.isSuccess()) {
			state = State.IN_SESSION;
			addLocationListener();
		} else {
			Toast.makeText(getApplicationContext(), "Please try again." , Toast.LENGTH_SHORT).show();
		}
		setButtionsAccordingToState();
	}
	
	private void denyInvitation() {
		state = State.START;
		setButtionsAccordingToState();
	}
	
	private void exitSession() {
		deleteSeesionOnServer();
		state = State.START;
		removeLocationListener();
		setButtionsAccordingToState();
	}
	
	private void deleteSession() {
		deleteSeesionOnServer();
		state = State.START;
		sessionStartedByUser = false;
		removeLocationListener();
		setButtionsAccordingToState();
	}
	
	private void viewMap() {
		Intent intent = new Intent(StartActivity.this, MapActivity.class);
		intent.putExtra("sessionId", sessionId);
		intent.putExtra("username", username);
		intent.putExtra("phoneNumber", phoneNumber);
		intent.putExtra("latitude", latitude);
		intent.putExtra("longitude", longitude);
		startActivity(intent);
	}
	
	private void sendLocation() {
		if (longitude == 0 && latitude == 0) {
			//no data collected yet
			return;
		}
		SendLocationRequest request = new SendLocationRequest();
		request.setName(username);
		request.setPhone(phoneNumber);
		request.setLatitude(Double.toString(latitude));
		request.setLongitude(Double.toString(longitude));
		server.sendLocation(request);
	}
	
	private void loadAppState() {
		SharedPreferences settings = getSharedPreferences(SharedPreferenceConstants.FILE_NAME, Context.MODE_PRIVATE);
		state = settings.getInt(SharedPreferenceConstants.STATE, State.START);
		sessionStartedByUser = settings.getBoolean(SharedPreferenceConstants.SESSION_STARTED_BY_CURRENT_USER, false);
		sessionId = settings.getString(SharedPreferenceConstants.SESSION_ID, null);
		username = settings.getString(SharedPreferenceConstants.CURRENT_USER_NAME, null);
		phoneNumber = settings.getString(SharedPreferenceConstants.CURRENT_USER_PHONE, null);
		if (username == null || phoneNumber == null) {
			updateUserData();
		}
	}
	
	private void storeAppState() {
		SharedPreferences settings = getSharedPreferences(SharedPreferenceConstants.FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(SharedPreferenceConstants.STATE, state);
		editor.putBoolean(SharedPreferenceConstants.SESSION_STARTED_BY_CURRENT_USER, sessionStartedByUser);
		editor.putString(SharedPreferenceConstants.SESSION_ID, sessionId);
		editor.commit();
	}
	
    private void addStartActions() {
		Button createSessionButton = new Button(this);
		createSessionButton.setText(R.string.button_create_new_session);
		createSessionButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(StartActivity.this, NewSessionActivity.class);
				startActivityForResult(intent, ActivtyRequestCode.FROM_NEW_SESSION_TO_START);
			}
		});
		Button checkForInvitation = new Button(this);
		checkForInvitation.setText(R.string.button_check_invitation);
		checkForInvitation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkIfAnInvitationWasSent();
			}
		});
		Button updateUserData = new Button(this);
		updateUserData.setText(R.string.button_update_user_data);
		updateUserData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				updateUserData();
			}
		});
		addButton(createSessionButton);
		addButton(checkForInvitation);
		addButton(updateUserData);
	}
	
	private void addInviteReceivedActions() {
		Button acceptInvite = new Button(this);
		acceptInvite.setText(R.string.button_accept_invitation);
		acceptInvite.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				acceptInvitation();
			}
		});
		Button denyInvite = new Button(this);
		denyInvite.setText(R.string.button_deny_invitation);
		denyInvite.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				denyInvitation();
			}
		});
		addButton(acceptInvite);
		addButton(denyInvite);
	}
	
	private void addInSessionActions() {
		Button viewMap = new Button(this);
		viewMap.setText(R.string.button_view_map);
		Button updateMap = new Button(this);
		updateMap.setText(R.string.button_update_map);
		Button sendLocation = new Button(this);
		sendLocation.setText(R.string.button_send_location);
		Button exitSession = new Button(this);
		exitSession.setText(R.string.button_exit_session);
		Button deleteSession = new Button(this);
		deleteSession.setText(R.string.button_delete_session);
		viewMap.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				viewMap();
			}
		});
		sendLocation.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				sendLocation();
			}
		});
		exitSession.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				exitSession();
			}
		});
		deleteSession.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				deleteSession();
			}
		});
		addButton(viewMap);
		addButton(updateMap);
		addButton(sendLocation);
		if (sessionStartedByUser) {
			addButton(deleteSession);
		} else {
			addButton(exitSession);
		}
	}
		
	
	private void addActionsAccordingToState() {
		if (state == State.START) {
			addStartActions();
		} 
		if (state == State.INVITE_RECEIVED) {
			addInviteReceivedActions();
		}
		if (state == State.IN_SESSION) {
			addInSessionActions();
		}
	}
	
	private void setButtionsAccordingToState() {
		removeOldButtons();
		addActionsAccordingToState();
		makeButtonsVisibleInButtonsLayout();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		server = new RemoteServer();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		loadAppState();
		setButtionsAccordingToState();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		storeAppState();
	}
}
