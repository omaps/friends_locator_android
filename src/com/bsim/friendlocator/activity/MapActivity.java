package com.bsim.friendlocator.activity;

import com.bsim.friendlocator.R;
import com.bsim.friendlocator.data.FriendLocation;
import com.bsim.friendlocator.restservices.executor.DummyServer;
import com.bsim.friendlocator.restservices.executor.Server;
import com.bsim.friendlocator.restservices.requestmessage.UpdateMapRequest;
import com.bsim.friendlocator.restservices.responsemessage.UpdateMapResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.annotation.TargetApi;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MapActivity extends ActionBarActivity {
	
	static final LatLng HAMBURG = new LatLng(53.558, 9.927);
	static final LatLng KIEL = new LatLng(53.551, 9.993);
	private GoogleMap map;
	
	private Server server;
	private String sessionId;
	private String username;
	private String phoneNumber;
	private double latitude;
	private double longitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_map, container,
					false);
			return rootView;
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		server = new DummyServer();
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
		        .getMap();
		
		readIntentData();
		
		UpdateMapResponse response = server.updateMap(createUpdateMapRequest());
		;
		for (FriendLocation friendLocator : response.getFriends()) {
			Double friendLat = Double.parseDouble(friendLocator.getLatitude());
			Double friendLog = Double.parseDouble(friendLocator.getLongitude());
			LatLng pos = new LatLng(friendLat, friendLog);
			map.addMarker(new MarkerOptions().position(pos).title(friendLocator.getName()));
		}
		
		if (response.getFriends().size() > 0) {
			FriendLocation first = response.getFriends().get(0);
			Double lat = Double.parseDouble(first.getLatitude());
			Double log = Double.parseDouble(first.getLongitude());
			LatLng firstPos = new LatLng(lat, log);
			 map.moveCamera(CameraUpdateFactory.newLatLngZoom(firstPos, 15));
			 map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
		}
		
	}
	
	
	private UpdateMapRequest createUpdateMapRequest() {
		
		UpdateMapRequest request = new UpdateMapRequest();
		request.setName(username);
		request.setPhone(phoneNumber);
		request.setSessionId(sessionId);
		return request;
	}
	
	private void readIntentData() {
		sessionId = getIntent().getExtras().getString("sessionId");
		username = getIntent().getExtras().getString("username");
		phoneNumber = getIntent().getExtras().getString("phoneNumber");
		latitude = getIntent().getExtras().getDouble("latitude");
		longitude = getIntent().getExtras().getDouble("longitude");
	}

}
