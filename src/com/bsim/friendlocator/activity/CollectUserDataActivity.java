package com.bsim.friendlocator.activity;

import com.bsim.friendlocator.R;
import com.bsim.friendlocator.R.id;
import com.bsim.friendlocator.R.layout;
import com.bsim.friendlocator.R.menu;
import com.bsim.friendlocator.constants.SharedPreferenceConstants;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.os.Build;

public class CollectUserDataActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_collect_user_data);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.collect_user_data, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_collect_user_data, container, false);
			return rootView;
		}
	}
	
	
	public void setUserData(View view) {
		EditText userName = (EditText) findViewById(R.id.UsernameEditText);
		EditText phone = (EditText) findViewById(R.id.UsernamePhone);
		
		String currentUserName = userName.getText().toString();
		String currentUserPhone = phone.getText().toString();
		
		SharedPreferences settings = getSharedPreferences(SharedPreferenceConstants.FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SharedPreferenceConstants.CURRENT_USER_NAME, currentUserName);
		editor.putString(SharedPreferenceConstants.CURRENT_USER_PHONE, currentUserPhone);
		editor.commit();
		
		finish();
	}

}
